import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './containers/Home/Home';
import Pricing from './containers/Pricing/Pricing';
import Checkout from './containers/Checkout/Checkout';
import Users from './containers/Users/Users';

function App() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/pricing" component={Pricing} />
      <Route path="/checkout" component={Checkout} />
      <Route path="/users" component={Users} />
    </Switch>
  );
}

export default App;
