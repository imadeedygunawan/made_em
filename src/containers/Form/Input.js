import React from 'react';

function Input(props) {    
    var required = false;

    if(props.required) {
        required = true;
    }

    if(props.type === "select") {
        return (
            <select className={props.className} id={props.id} name={props.name} required={required} >
                <option defaultValue="">Choose...</option>
                {
                    props.selectOptions.map(option => <option key={option} value={option}>{option}</option>)
                }
            </select>
        );
    }
    else {
        return (
            <input type={props.type} name={props.name} className={props.className} id={props.id} placeholder={props.placeholder} defaultValue={props.defaultValue} required={required} />
        );
    }    
}

export default Input;