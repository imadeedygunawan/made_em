import React from 'react';
import Label from './Label';
import Input from './Input';
import Invalidfeedback from './Invalidfeedback';

function Groupfield(props) {
    if(props.label && props.invalidText) {
        return (
            <>
                <Label id={props.id} label={props.label} labelClassName={props.labelClassName} />
                <Input type={props.type} id={props.id} className={props.className} placeholder={props.placeholder} defaultValue={props.defaultValue} required={props.required} selectOptions={props.selectOptions} />
                <Invalidfeedback invalidText={props.invalidText} />
            </>
        );
    }
    else if(props.label) {
        return (
            <>
                <Label id={props.id} label={props.label} labelClassName={props.labelClassName} />
                <Input type={props.type} id={props.id} className={props.className} placeholder={props.placeholder} defaultValue={props.defaultValue} required={props.required} selectOptions={props.selectOptions} />
            </>
        );
    }
    else if(props.invalidText) {
        return (
            <>
                <Input type={props.type} id={props.id} className={props.className} placeholder={props.placeholder} defaultValue={props.defaultValue} required={props.required} selectOptions={props.selectOptions} />
                <Invalidfeedback invalidText={props.invalidText} />
            </>
        );
    }
    else {
        return (
            <>
                <Input type={props.type} id={props.id} className={props.className} placeholder={props.placeholder} defaultValue={props.defaultValue} required={props.required} selectOptions={props.selectOptions} />
            </>
        );
    }
}

export default Groupfield;