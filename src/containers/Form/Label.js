import React from 'react';

function Label(props) {
    return (
        <label className={props.labelClassName} htmlFor={props.id}>{props.label}</label>
    );
}

export default Label;