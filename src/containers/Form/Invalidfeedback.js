import React from 'react';

function Invalidfeedback(props) {
    return (
        <div className="invalid-feedback">{props.invalidText}</div>
    );
}

export default Invalidfeedback;