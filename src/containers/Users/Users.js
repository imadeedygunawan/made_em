import React from 'react';
import axios from 'axios';

export default class Users extends React.Component {
    state = {
      users: [],
      isLoading: true,
      error:null
    }

    renderUsers(data, peco) {
        if(typeof data == "object") {
            var ulKey = peco+data.id;

            return (
                <ul key={ulKey} className={peco}>
                {
                    Object.keys(data).map(
                        sub => (typeof data[sub] == "object")
                            ? this.renderUsers(data[sub], sub)
                            : <li key={sub}><b>{sub}: </b>{data[sub]}</li>
                    )
                }
                </ul>
            )
        }
        else {
            return '';
        }
    }
  
    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then(res => {
            this.setState({ 
                users: res.data,
                isLoading: false
            });
        })
        .catch(error => {
            this.setState({ error, isLoading: false })
        })
    }
  
    render() {
        const { isLoading, users } = this.state;
        return (
            <>
                <header>
                    <div className="collapse bg-dark" id="navbarHeader">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-8 col-md-7 py-4">
                                    <h4 className="text-white">About</h4>
                                    <p className="text-muted">Add some information about the album below, the author, or any other background context. Make it a few sentences long so folks can pick up some informative tidbits. Then, link them off to some social networking sites or contact information.</p>
                                </div>
                                <div className="col-sm-4 offset-md-1 py-4">
                                    <h4 className="text-white">Contact</h4>
                                    <ul className="list-unstyled">
                                        <li><a href="/" className="text-white">Follow on Twitter</a></li>
                                        <li><a href="/" className="text-white">Like on Facebook</a></li>
                                        <li><a href="/" className="text-white">Email me</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="navbar navbar-dark bg-dark shadow-sm">
                        <div className="container d-flex justify-content-between">
                            <a href="/" className="navbar-brand d-flex align-items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" aria-hidden="true" className="mr-2" viewBox="0 0 24 24" focusable="false"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                                <strong>Album</strong>
                            </a>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                        </div>
                    </div>
                </header>
                <main role="main">
                    <div className="container">
                        <h2>Users list from https://jsonplaceholder.typicode.com/users</h2>
                        <br/>
                        {
                            !isLoading 
                            ?   users.map(
                                    user => this.renderUsers(user, "user_container")
                                )
                            :   <li key="error">Loading...</li>
                        }
                    </div>
                </main>
            </>
        )
    }
  }