import React from 'react';
import GroupField from '../Form/GroupField';
import Label from '../Form/Label';
import Input from '../Form/Input';

function Checkout() {
  return (
    <div className="container">
      <div className="py-5 text-center">
        <img className="d-block mx-auto mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
        <h2>Checkout form</h2>
        <p className="lead">Below is an example form built entirely with Bootstrap’s form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>
      </div>

      <div className="row">
        <div className="col-md-4 order-md-2 mb-4">
          <h4 className="d-flex justify-content-between align-items-center mb-3">
            <span className="text-muted">Your cart</span>
            <span className="badge badge-secondary badge-pill">3</span>
          </h4>
          <ul className="list-group mb-3">
            <li className="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 className="my-0">Product name</h6>
                <small className="text-muted">Brief description</small>
              </div>
              <span className="text-muted">$12</span>
            </li>
            <li className="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 className="my-0">Second product</h6>
                <small className="text-muted">Brief description</small>
              </div>
              <span className="text-muted">$8</span>
            </li>
            <li className="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 className="my-0">Third item</h6>
                <small className="text-muted">Brief description</small>
              </div>
              <span className="text-muted">$5</span>
            </li>
            <li className="list-group-item d-flex justify-content-between bg-light">
              <div className="text-success">
                <h6 className="my-0">Promo code</h6>
                <small>EXAMPLECODE</small>
              </div>
              <span className="text-success">-$5</span>
            </li>
            <li className="list-group-item d-flex justify-content-between">
              <span>Total (USD)</span>
              <strong>$20</strong>
            </li>
          </ul>

          <form className="card p-2">
            <div className="input-group">
              <Input id="promo-code" type="text" className="form-control" placeholder="Promo code" />
              <div className="input-group-append">
                <button type="submit" className="btn btn-secondary">Redeem</button>
              </div>
            </div>
          </form>
        </div>
        <div className="col-md-8 order-md-1">
          <h4 className="mb-3">Billing address</h4>
          <form className="needs-validation" noValidate="">
            <div className="row">
              <div className="col-md-6 mb-3">
                <GroupField id="firstName" type="text" className="form-control" placeholder="" defaultValue="" required="required" label="First name" invalidText="Valid first name is required." />
              </div>
              <div className="col-md-6 mb-3">
                <GroupField id="lastName" type="text" className="form-control" placeholder="" defaultValue="" required="required" label="Last name" invalidText="Valid last name is required." />
              </div>
            </div>

            <div className="mb-3">
              <Label id="username" label="Username"/>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">@</span>
                </div>
                <GroupField id="username" type="text" className="form-control" placeholder="Username" defaultValue="" required="required" invalidText="Valid username is required." />
              </div>
            </div>

            <div className="mb-3">
              <GroupField id="email" type="text" className="form-control" placeholder="you@example.com" defaultValue="" label="Email (Optional)" invalidText="Please enter a valid email address for shipping updates." />
            </div>

            <div className="mb-3">
              <GroupField id="address" type="text" className="form-control" placeholder="1234 Main St" defaultValue="" label="Address" invalidText="Please enter your shipping address." />
            </div>

            <div className="mb-3">
              <GroupField id="address2" type="text" className="form-control" placeholder="Apartment or suite" defaultValue="" label="Address 2"/>
            </div>

            <div className="row">
              <div className="col-md-5 mb-3">
                <GroupField id="country" type="select" className="custom-select d-block w-100" label="Country" selectOptions={['United State', 'Indonesia']} invalidText="Please select a valid country." required="required" />
              </div>
              <div className="col-md-4 mb-3">
                <GroupField id="state" type="select" className="custom-select d-block w-100" label="State" selectOptions={['California', 'Bali']} invalidText="Please provide a valid state." required="required" />
              </div>
              <div className="col-md-3 mb-3">
                <GroupField id="zip" type="text" className="form-control" placeholder="" label="Zip" invalidText="Zip code required." required="required"/>
              </div>
            </div>
            <hr className="mb-4" />
            <div className="custom-control custom-checkbox">
              <Input id="same-address" type="checkbox" className="custom-control-input" />
              <Label id="same-address" labelClassName="custom-control-label" label="Shipping address is the same as my billing address" />
            </div>
            <div className="custom-control custom-checkbox">
              <Input id="save-info" type="checkbox" className="custom-control-input" />
              <Label id="save-info" labelClassName="custom-control-label" label="Save this information for next time" />
            </div>
            <hr className="mb-4" />

            <h4 className="mb-3">Payment</h4>

            <div className="d-block my-3">
              <div className="custom-control custom-radio">
                <Input id="credit" type="radio" name="paymentMethod" className="custom-control-input" />
                <Label id="credit" labelClassName="custom-control-label" label="Credit card" />
              </div>
              <div className="custom-control custom-radio">
                <Input id="debit" type="radio" name="paymentMethod" className="custom-control-input" />
                <Label id="debit" labelClassName="custom-control-label" label="Debit card" />
              </div>
              <div className="custom-control custom-radio">
                <Input id="paypal" type="radio" name="paymentMethod" className="custom-control-input" />
                <Label id="paypal" labelClassName="custom-control-label" label="Paypal card" />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 mb-3">
                <GroupField id="cc-name" type="text" className="form-control" label="Name on card" invalidText="Name on card is required" placeholder="" required="required" />
                <small className="text-muted">Full name as displayed on card</small>
              </div>
              <div className="col-md-6 mb-3">
                <GroupField id="cc-number" type="text" className="form-control" label="Credit card number" invalidText="Credit card number is required" placeholder="" required="required" />
              </div>
            </div>
            <div className="row">
              <div className="col-md-3 mb-3">
                <GroupField id="cc-expiration" type="text" className="form-control" label="Expiration" invalidText="Expiration date required" placeholder="" required="required" />
              </div>
              <div className="col-md-3 mb-3">
                <GroupField id="cc-cvv" type="text" className="form-control" label="CVV" invalidText="Security code required" placeholder="" required="required" />
              </div>
            </div>
            <hr className="mb-4" />
            <button className="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
          </form>
        </div>
      </div>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">© 2017-2019 Company Name</p>
        <ul className="list-inline">
          <li className="list-inline-item"><a href="/">Privacy</a></li>
          <li className="list-inline-item"><a href="/">Terms</a></li>
          <li className="list-inline-item"><a href="/">Support</a></li>
        </ul>
      </footer>
    </div>
  );
}

export default Checkout;
